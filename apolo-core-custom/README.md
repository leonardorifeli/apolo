Este projeto possui artefatos que normalmente devem ser alterados pelo desenvolvedor e de certa forma, o core
do sistema depende dele.

A ideia deste isolamento é que o desenvolvedor que for utilizar o framework não precise alterar a estrutura
principal do sistema, podendo assim, isolar suas alterações e conservando a estrutura CORE do sistema 
intácta.


