Este projeto WEB utiliza somente anotações para realizar sua configuração de start up.

Um ponto muito importante na hora de colocar a aplicação para funcionar é não esquecer de colocar o
parâmetro informando qual ambiente está sendo executado na chamada do servidor.

-Dappconfig=sua_config

onde sua_config é uma pasta que fica em src/main/resources deste projeto.