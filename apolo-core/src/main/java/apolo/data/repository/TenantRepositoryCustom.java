package apolo.data.repository;

import apolo.data.model.Tenant;

public interface TenantRepositoryCustom extends BaseRepository<Tenant> {

}
