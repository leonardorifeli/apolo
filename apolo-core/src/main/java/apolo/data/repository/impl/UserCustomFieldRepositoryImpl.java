package apolo.data.repository.impl;

import apolo.data.model.UserCustomField;
import apolo.data.repository.UserCustomFieldRepositoryCustom;

import org.springframework.stereotype.Repository;

@Repository
public class UserCustomFieldRepositoryImpl extends BaseRepotitoryImpl<UserCustomField> implements UserCustomFieldRepositoryCustom {

}